---
layout: post
title: "Some useful tips and tools for Ubuntu"
date: 2018-06-04
categories: linux
---

Ubuntu is an awesome distribution and one of its goals is to be as user-friendly as possible.
While a fresh installation is ready to use, you can still tweak it to fit your needs.
The following 10 tools might be a useful addition to your computer.

<!--more-->


## tilda

Using `tilda`, your terminal is always only one button away from you.
Upon pressing it, a small, customizable terminal window pops up and auto-hides when you don't need it anymore.

**Update:**
While `tilda` has some very nice features, a drop-down terminal can also be achieved in an easier fashion.
Simply install `xfce4-terminal` ([unless you already use xfce]({{ site.baseurl }}{% post_url 2018-05-21-xfce-perfect-desktop-environment %})) and bind `xfce4-terminal --drop-down` to your desired key.


## plank

Plank is a very lightweight dock that is highly customizable.
You can put starters and files on there, and even tools like a clock.
Those who don't like the classic menubar can use this package to have a more mac-like feeling.


## gnome-tweak-tool

As I have mentioned in another article, I personally don't like Gnome.
However, plenty of people use it, and the `gnome-tweak-tool` is an essential package.
With this program, themes like cursor and icons or the position of the window buttons can be configured.
It also adds some nice features that weren't there before:
For example, one can remap the caps-lock button to become an additional shift-, control- or escape-key, which is especially useful for `vim` users.


## compiz

Of course, this list can't be complete without the `compiz` package.
This one adds beautiful animations to your desktop, like window snapping, desktop wall and fading on minimizing.
You won't recognize the look and feel anymore - especially when using the wobbly window feature.


## sudo visudo

With this command, you can edit a configuration file.
Add the entry `Defaults pwfeedback` to it. From now on, the password prompt in the terminal (e.g. when using `sudo`) will show asterisks (`***`).
This is helpful to prevent double-pressing a key.
More information can be found [in the Ubuntu documentation](https://help.ubuntu.com/community/Sudoers#Enabling_Visual_Feedback_when_Typing_Passwords).


## zathura

This program is an extremely fast and lightweight PDF viewer.
When compared with `evince`, `zathura` wins by miles:
It can load even very big PDF files fastly, and scrolling is smooth anyways.
Another cool feature is handy for `vim` users - many known key combinations (like the `hjkl` keys, or `:q`) work here as well.


## wine

It shouldn't even be mentioned as additional tool, I think it should directly come with a fresh installation:
`wine`. With `wine`, you can run many windows programs under Linux - and as its name says, *w*ine *i*s *n*ot an *e*mulator.
They also list the compatibility with programs on their website.

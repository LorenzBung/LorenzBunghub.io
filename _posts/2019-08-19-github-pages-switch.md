---
layout: post
title: "Why I switched my blog host to GitHub"
date: 2019-09-07
category: software
image: "/media/github-pages.png"
---

If you visit [my homepage]({{ site.baseurl }}) frequently, you might have noticed a change in design, speed and other aspects.
The reason is that I switched over to GitHub pages, and here's why.

<!--more-->


I started to use my website and write this blog in march of 2018 by buying a cheap webserver and domain.
It seemed like a good deal: around 20€ per year for a domain and a webserver with Wordpress.
However, that ended up to be a big misconception!


## Insane load times and server downtimes

The host I had my blog on automatically restarted my webserver every night at 3am CET.
This lead to a huge downtime of 30 minutes per day, which is especially bad if you have an international readership.

Another reason was the bad performance:
The landing page used to load for 3 seconds upwards, even though the server was located in Frankfurt, Germany.

Thirdly, Wordpress has a big overhead to it.
While it offers a lot of benefits like plugins, themes and regular security updates, it is still a very big CMS in the background of your blog.


## GitHub Pages: Benefits and disadvantages

Hosting on GitHub doesn't only has its good sides, but certainly a few very nice things to offer.
The benefits include:

- extremely fast load speeds, due to static page generation with [Jekyll](https://jekyllrb.com/)
- free hosting
- source code can be hidden if you have a GitHub pro account
- high security, because there is no CMS and PHP in the background
- free use of the GitHub CDN which leads to better ping, load time and availability
- high maintainability due to easy use of git
- your content stays yours - you know where it is located

Some of the negatives are:

- no webserver access, so no serverside scripts like PHP, python, ...
- worse search engine ranking, even though `jekyll-seo-tag` helps a lot
- no CMS (at least if you don't consider GitHub a CMS), also no WYSIWYG


## The process of switching over

First, I had to convert all my blogposts to Markdown.
I know there are scripts to handle this task, but for me it was a great way to review and fix typos.
After that, I had to setup jekyll and the GitHub repository, and change all links in my posts to jekyll's liquid tags.
I also had to re-include the images used.

So, would I do it again or recommend it to other people?
GitHub pages is especially useful for developers who use git anyways.
Also, the free (and very good) hosting is a major benefit.
However, not having a good search engine ranking is a major flaw.
So yes, I would definitely do it again - but I understand that it is not the right thing for everyone.

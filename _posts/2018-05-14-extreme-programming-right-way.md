---
layout: post
title: "Extreme Programming - the right way to go"
date: 2018-05-14
categories: software
---

If you have never heard of it, Extreme Programming (or XP) is a programming methodology to improve software quality and maintainability.
In this article, I will share my view on it, what I do and what I dislike and how you should use it.
You can read more about it in [this Wikipedia article](https://en.wikipedia.org/wiki/Extreme_programming).

<!--more-->


## Test-driven development

One of the main methods in extreme programming is test-driven development.
This means, the software developers first write tests, and then go ahead to actually implement the desired functionality.
The goal of this is to detect mistakes and bugs early on.
Also, it makes programmers think about what they are doing *before* doing something potentially wrong.

Now don't get me wrong:
writing tests during development is absolutely necessary and helpful.
Doing so ensures high-quality code and lets you test newly implemented methods thoroughly.
However, it's mostly just unnecessary and obsolete work.
For example, imagine having this constructor:

```java
public Circle(int radius, int centerX, int centerY, String color);
```

Following the XP pattern, you'd have to create loads of circles now to make sure everything works properly:

```java
public void negativeRadius();
public void negativeCenterX();
public void unknownColorString();
```

And this is just for a simple constructor of a circle!
Now do that for a more complex constructor, or every method you write.
In most of the cases, these tests are tedious and don't show possible bugs.
Consequently, writing them is a waste of time; nobody benefits from it.


## Simplicity

Following the KISS-pattern, Extreme Programming proposes light code, including only the minimal necessary functionality.

It has often been criticized that following this paradigm can't lead to a far-sighted project and will most likely result in a work overload.
While I can understand this point and see the problems named there, the benefits simply outweigh the downsides.

In software development, the key factors are readability and maintainability.
After finishing a project, keeping it in shape, fixing bugs and adjusting it to newer needs becomes the main task.
Having understandable code is a huge benefit there:
It helps new team members to understand the existing code base quickly and older members follow the thoughts of their co-workers.
Keeping your code simple ensures that, and especially on a quickly-changing team becomes essential.


## How you should use extreme programming

As its name says, extreme programming takes programming paradigms to the extreme.
There are certainly a lot of benefits to that, however one has to find the right degree to follow the patterns.
But whether you're a friend of quickly-changing and agile software development or like to build with the future in hindsight, you can certainly learn a lot from XP (and if it's just how *not* to do it).

Some paradigms should be used by *every single team*:
Unit tests, refactoring, simplicity and continuous integration are just a few of them.
There are many more out there, and each and every one suits one or another team better.

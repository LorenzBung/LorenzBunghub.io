---
layout: post
title: "Ubuntu 18.04 \"Bionic Beaver\" release"
date: 2018-04-26
categories: linux
image: "https://upload.wikimedia.org/wikipedia/commons/9/9d/Ubuntu_logo.svg"
---

In case you've missed it, today (26.04.2018) is the launch day of Ubuntu 18.04 LTS.
This is great news, for one because it's always exciting to explore something new which will be around for the next five years, but more importantly, because it has a lot of improvements and fixes I really enjoy.

<!--more-->


## Unity dropped in favor of Gnome

Unity has been the desktop environment of Ubuntu for quite a while now.
In fact, it was since I am familiar with Linux Systems (which is not too long, but since 12.04 LTS came out).

Now this is a really major change to look and feel.
Many people didn't like Unity, and found it really clunky.
I personally never had an issue with Unity itself, but what I didn't like was how impossible it was to customize it.
Gnome is [not my favourite environment]({{ site.baseurl }}{% post_url 2018-05-21-xfce-perfect-desktop-environment %}) too, at least I do not like the application menu.

But I'm still looking forward to it for a number of reasons:


### Customizability

Gnome is not the go-to DE if you want a lot of options.
Xfce and KDE exceed both Gnome and Unity by miles in that regard.
However, there is `gnome-tweak-tool`, designed to configure the Gnome environment.
Also, with `gconf`, one can easily change all settings Gnome has to offer.

Unity, on the other hand, is based on Gnome (and thus you can use the said tools), but also offers `unity-tweak-tool`.
While it offers some additional options, it also overrides / blocks some that you get from Gnome.
In addition to that, you get:


### Bugs

There are so many bugs caused by Unity interfering with Gnome, I won't even begin to name all of them.

For example, the positioning of the window-control-buttons (minimize, maximize, close) doesn't work in Ubuntu.
Or, choosing a pointer theme (like `Dmz-black`) changes nothing.

If one starts playing around with `compizconfig-settings-manager` (which gives you a better window composition), things start to get *really* messy.
There are patches for both Gnome and Unity, which at first you have no idea is the right one.
Finally, you figure out which ones you *should* use, and it still doesn't work, or give you really weird bugs!



There are many more reasons to not like Unity, which I will not cover here.
However, it doesn't matter anyways from now on, since Unity is dead.


## Minimal installation of Ubuntu

Something that always really bothered me about Ubuntu was the amount of packages that shipped with it.
I even considered switching over to another distribution (such as Arch), because my PC always felt "bloated", even though I really like the Ubuntu community and software.

With this update, there will be an option on installation to do a minimal installation.
This will drop stuff like the LibreOffice software and other unnecessary things, which a lot of users won't need.
Of course one could do a "minimal installation" before, it just was so much work.
You had to do a server-installation first, which gives you no desktop environment, and then install all the needed packages from the console.

But, if a user is skilled enough to do that, why not choose Arch as his distro right away?
Luckily, this problem exists no longer, and fresh installations of Ubuntu will become cleaner than ever before.


There are a lot of more things this update brings, such as a newer kernel, security updates and others, but those are the topics that I find the most interesting about it.
If you want to read more about them, you can read about the [release notes](https://wiki.ubuntu.com/BionicBeaver/ReleaseNotes).

To download the new version, visit [the official download page](https://ubuntu.com/download/desktop), and if you already have Ubuntu installed, you can upgrade with `sudo apt update && sudo apt do-dist-upgrade`.

---
layout: post
title: "The end of Windows 7"
date: 2019-11-21
category: linux
image: "https://logodix.com/logo/69913.png"
---

Windows 7 has been a very important operating system to me.
Even though I'm now a convinced linux user, it still had a big impact on me.
Now, the support is nearing its end - and that's a great opportunity.

<!--more-->


For many millenials, Windows XP, but mainly Windows 7 were the first operating systems they ever used.
The same goes for me: While my parent's PC still had Windows XP, my first laptop came with a brand-new version of Windows 7 Home Premium installed.
For that reason, it will always have a special meaning to me.


## What Windows 7 did right

Many problems that prior versions of the operating systems had were fixed in this version.
For example, it was the first Windows version to come with a full 64-bit support, which finally raised the limitations on RAM.
Also, a lot of bugs that were present in Vista, the previous Windows, were finally fixed in 7.
This also meant a lot less memory-hungry system, but also far fewer bluescreens and other crashes (which were actually quite frequent to encounter in XP).

But the point with the biggest impact on me is yet to be named:
Windows 7 was the system that introduced me into the world of technology.
It was the system that I spent countless hours tweaking to my needs and
played Minecraft, Skyrim and other games on,
explored the depths of the internet with,
and of course made my first programming experiences on.

This and many more arguments are the reason I am still, to this day, using Windows 7 on my main computer.
However, even though I've grown to be so familiar with it, it still is a good thing to see it go after all this time.


## Why it is necessary to say goodbye

While there have been many amazing things about Windows 7, we still have to accept the fact that its support is ending.
That's not a bad thing, however: There are a lot of benefits in it too.

Firstly, Windows 7 has aged.
There are a lot of security flaws in the system, and since there won't be any more security updates, it's a high risk to still run this system.
Also, Windows 10 has taken the place that 7 once had: It combines the good aspects of 7 with the new knowledge and features of today's hard- and software.
Speaking of which: The glass-design was good in 2015, but it looks really outdated now.

Another reason is, that it is still a closed-source operating system.
Yeah, sure, we all had great experiences with it.
But [free software]({{ site.baseurl }}{% post_url 2018-05-28-free-software %}) is more secure and flexible: You have the full control over the system you're running.


## Time to move forward

There were only two reasons that held me back from switching to a linux-based OS on my main computer.
The first was *laziness*: I just didn't want to bother backing up my data, reinstalling my OS and finally trying to get it all to work on Ubuntu or the like.

The second, much bigger problem was *compatibility*.
I didn't want to give up on many of the windows-exclusive programs, mainly on games.
With games, it's especially hard to get them to run smoothly via WINE.
But since [Vulkan](https://en.wikipedia.org/wiki/Vulkan_(API)) and Valve's [Proton](https://github.com/ValveSoftware/Proton), it has become so much easier to achieve good performance through WINE.

So you could say that the end of support is an opportunity for change - which is definitely a good thing.
I for one will make use of it and switch over to Linux completely.

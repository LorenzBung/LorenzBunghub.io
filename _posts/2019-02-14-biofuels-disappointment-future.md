---
layout: post
title: "Biofuels: disappointment or possible future?"
date: 2019-02-14
categories: environment
image: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/0305_Rapsfeld_HRO-HST_PICT4705.JPG/1024px-0305_Rapsfeld_HRO-HST_PICT4705.JPG"
---

In our current environment, sustainability becomes more and more important.
Biofuels offer a climate-friendly option in an energy-hungry society.
But are they really a possible future, or do the negatives outweigh their benefits?

<!--more-->


## Difficulties in the current situation

Our society is highly dependant on fossil energies.
Not only gas and diesel are made out of mineral oils, but also all the plastic we use.
There are several problems with that:

Firstly, oils are getting rare and expensive.
The states in the gulf region have nearly monopolized them, and the remaining reserves are shrinking more and more.
So, moving away from them is unevitable.

Secondly, the use of mineral oils is really bad for the climate.
Large amounts of CO2 are released by fossil fuels, and this impacts the global warming a lot.
By switching over to biological alternatives, we could [reduce our ecological footprint]({{ site.baseurl }}{% post_url 2018-06-19-ecological-footprint %}).


## Why biofuels might be a solution

The main benefit over traditional fossil resources is, that biofuels are basically CO2-neutral.
For example, rape oils provide a great way to power diesel machines.
When burned, those biological diesel-alternatives still produce a lot of CO2.
But during the life cycle of the plant, it absorbs said amount.
Thus, there are effectively no emissions of environmentally harming gases.


## The problem: Are they not that green after all?

While there certainly are a good amount of benefits, there is also [a list of issues](https://en.wikipedia.org/wiki/Issues_relating_to_biofuels) related to this new technology.
One point critics make is that those plants take up space, which could be used to grow crops.
While this is true, there is still a huge amount of abundance in food, at least in the industrial world.

However, often rainforest has to be cleared to make new land for biofuel plants.
This is a far bigger issue:
Not only does the rainforest itself absorb a lot of CO2; it also represents a habitat to a lot of different species (some of which are near to extinction).
Studies have shown that this impacts the CO2-effectiveness of biological energy sources massively.


## The right way to use

Biological alternatives to fossil fuels offer great opportunities.
In the long run, we won't be able to rely on those sources anyways, and ecologically it is more than time to switch to a better solution.
However, it is crucial to use the given technologies correctly.
Making new land in the rainforest will benefit neither the environment nor us as humans.
Changing our culture as well as our known habits however, will.

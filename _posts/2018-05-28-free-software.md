---
layout: post
title: "Free software - about the GNU project"
date: 2018-05-28
categories: software
image: "https://upload.wikimedia.org/wikipedia/commons/2/22/Heckert_GNU_white.svg"
---

Software, developed by companies or by volunteers - can be of two sorts.
It either is free (as in freedom) or proprietary.
In most cases, companies choose the latter - because they can make much more profit off the product.
Famous for that are for example the Adobe software, Google Chrome, Windows and many, many more.
Free software, on the other hand, allows its users to view and edit the source code freely - examples are Emacs, the Linux kernel and other programs.

<!--more-->


## The GNU project

The [GNU project](https://www.gnu.org) aims to make all software free - and prevent the development of proprietary software.
Note that the word free isn't referring to the *price* of the product, which can be any amount.
It's rather referring to the freedom, which the software grants the users.
This freedom is split in 4 aspects:

1. The freedom to run the program as you wish, for any purpose.
2. The freedom to study how the program works, and change it so it does your computing as you wish.
3. The freedom to redistribute copies so you can help others.
4. The freedom to distribute copies of your modified versions to others.

While the first aspect is present in free and proprietary software, the former cuts the other 3 freedoms from the user.


## Always use free software if possible

Supporters of GNU refuse to use non-free software, because it is an ethical issue for them.
While I see the reasoning behind this, I don't think it is the optimal way to deal with the problem.
Sometimes, there simply is no way around it.
In my opinion, the correct way to approach the issue is the following:

1. *Contribute* to free software.
   Doing so will result in a broader field of even better software, form which everyone will benefit.
2. *Use* free software if possible.
   If you have the choice between OpenOffice and Word, choose the former.
   If you have the choice between Photoshop and Gimp, use Gimp.
3. *There is no way around it*:
   In case you really have to, fall back on proprietary software.
   Sometimes you have no other choice:
   Your Nvidia driver for example.

A good start is using and contributing to a GNU/Linux distribution, for example [Ubuntu]({{ site.baseurl }}{% post_url 2018-04-26-ubuntu-18-04-bionic-beaver-release %}).
This distribution in particular is not recommended by the GNU project; the reason is, that you *can* use non-free software with it.
However, if you don't use the `restricted` repository (which I recommend), everything will be free.


## Final words

Proprietary software is everywhere - and too often, there is no way around it.
Limit your use of it, and dislike using it.
If you can (and I'm sure you do!), contribute to free software in some way - may it be by writing documentation, translating or even maintaining a package.
Always keep in mind, that non-free software is potentially harmful - don't forget that.

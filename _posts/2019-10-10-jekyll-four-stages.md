---
layout: post
title: "Four stages of using jekyll"
date: 2019-10-10
category: software
image: "https://jekyllrb.com/img/logo-2x.png"
---

Jekyll is an awesome static-site generator.
It is also installed on GitHub Pages by default.
However, you have to get into it - and this are the stages when using it.

<!--more-->


## Stage 1: Pure confusion

At first, you probably won't understand anything.
At least, that's how I felt - I was overwhelmed!
But while one will feel this way with most new frameworks at first, their benefits only become clear after some time.


At first, I only pushed static HTML pages to my repository.
That did actually work - it is the simplest way of using jekyll.
I already used GitHub Pages this way, in an old repository.
However, since I wanted to [move my blog from Wordpress]({{ site.baseurl }}{% post_url 2019-08-19-github-pages-switch %}), I wanted to get the most out of it.


## Stage 2: Understanding the post system

So I started to read the documentation and take a look at some examples.
The first thing I learned about was the folder structure that jekyll utilizes.

```
.
+-- _drafts
|   +-- 2019-09-14-some-draft.md
|   +-- 2019-09-18-another-draft.md
+-- _posts
|   +-- 2019-10-10-jekyll-four-stages.md
|   +-- 2019-08-19-github-pages-switch.md
+-- media
|   +-- jekyll-logo.png
|   +-- github-logo.jpeg
+-- about.md
+-- index.md
```

Jekyll takes every file in the `_posts`-directory and tries to build a static page out of it.
The easiest way is to write in Markdown, but one can also straight-up use HTML.
Drafts (which should not be posted yet) can be placed in the `_drafts`-directory.
Files in the base directory are not interpreted as posts, but rather as static pages on your site (see `about.md` and `index.md`).

You can also create your own directories (like `media` here).
Jekyll will not show pages, posts or whatever lies in there by default, but you can still link to those pages.
This is useful for example if you want to link to a pdf, but not have the link show up on your main page.


## Stage 3: Configuring config.yml, layouts, includes etc.

Once you know about how basic posting works, you'll probably want to configure your site a little bit.
In the `_config.yml` file in the base directory, site-wide settings can be set.
This ranges from basic options like the title and description of your website to far more interesting and advanced topics, such as using plugins for SEO or verifying your site ownership at Google.

You can also adjust layouts used in your posts and/or pages, by modifying the `_layouts` folder.
Sometimes you'll also need to tinker with the `_includes`, which helps to keep your layout structure clean and simple.

Overall, configuring jekyll is the really fun part of it.
There are so many options to play with, and the sky is really the limit!


## Stage 4: Using Liquid templates

The feature for generated content that jekyll offers is [the Liquid templating language](https://jekyllrb.com/docs/liquid/).
This part is where it gets really fun, because you can do so much with this feature.
Apart from basic usages like using the post date etc., you have logic operations and access to loops and variables.

I used liquid to check for a "hidden" flag in my page frontmatter.
The reason is that I wanted to hide pages from the menu bar, but still be able to link to them and have them in my base directory.
It's actually quite simple to check for this!
In `_includes/header.html`, put the following code:

{% raw %}
```
{%- if my_page.title and my_page.hidden != true -%}
<a class="page-link" href="{{ my_page.url | relative_url }}">{{ my_page.title | escape }}</a>
{%- endif -%}
```
{% endraw %}


## Conclusion

Jekyll is an awesome tool, and in combination with GitHub it makes creating a highly-configurable site really easy.
Playing around with the different settings and features is what makes it so great.
And the best, like with most things in life, is:
The more you use it, the better you get at it.

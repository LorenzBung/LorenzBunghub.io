---
layout: post
title: "Geocaching: 1000 finds"
date: 2019-09-12
category: hobby
image: "https://live.staticflickr.com/5218/5488778377_e76bc3e2b3_b.jpg"
---

You might know I'm also an active [Geocacher](https://www.geocaching.com).
In the past two years, my profile (which you can find in my [about page]({{ site.baseurl }}/about)) has accumulated over 1000 finds.

<!--more-->


## What is geocaching?

If you've never heard of geocaching, I'll shortly explain what it's all about.
Geocachers search for boxes (the "caches"), hidden by other cachers.
The boxes contain a piece of paper, on which the finder has to note the date and their geocaching username.
After finding a cache, it is also logged online to its respective listing page.

Most of the times, the listing page will give you the coordinates to the hidden box.
Those caches are called "Traditionals", and there are the most frequent type of caches.
However, for other types, the coordinates are not available immediately.
You might have to solve a puzzle in order to decode them (Mystery caches), or visit other places in the real world and gather information (Multi caches).

So what's so fun about that?
Well, there's a few things that really excite me about it!

- Finding a cache is always an adventure.
  You never know exactly what you're getting into, and sometimes, there's a surprise waiting for you!
- You are out in the nature and besides the search for the box, it's just enjoyable to enjoy the sun and wind (and sometimes rain too).
- Often, caches are hidden in spectacular places.
  That might be somewhere with an amazing view, or inside of a misty forest.
  The amount of places I've seen thanks to geocaching is astounding.


## A hobby for everyone

Another thing that I really like about geocaching is how it is a hobby suited for everybody:

- You might be a very social person - there are events to meet new people.
- It's a rainy day and you'd rather stay inside?
  Just solve a mystery - and go to the coordinates you got later on.
- Or maybe you care a lot for nature and your surroundings:
  CITO events are organized to keep forests, rivers and other places clean.


## 1000 great experiences

So after finding 1000 caches, there are a lot of experiences I made.
However, it won't stop here.
There is still so much to explore - and every single cache will teach me something about the world I live in.

---
layout: post
title: "How do you keep your ecological footprint low"
date: 2018-06-19
categories: environment
image: "/media/sapling.jpg"
---

We live in a time where ecological thinking has become more and more important.
Since global warming and CO2 are a growing danger, we need to be more aware of our environment.
But how can one effectively live more "green" and leave a positive impact on the world?
The ecological footprint plays a great role there.

<!--more-->


## Global warming is a huge danger

I know, I know. You get told about it every single day.
It's in the news, the internet, everywhere really.
The topic sounds dull or even outright *boring* to us nowadays.
But how can it be that we still haven't made a change?
Every person I know, me included, need to make a significant change to their lifestyle in order to reduce CO2 emission.


### What is this "ecological footprint" you're talking about?

Okay, so in the unlikely case you haven't already heard about it, I'll first explain what the ecological footprint is and why it is so important.

Every human produces CO2:
By consuming food, building houses, making fire and mainly, transportation.
This causes its amount in the atmosphere to rise - which is a bad thing.
The so-called "ecological footprint" is the amount of CO2 produced by one single human being, and the goal is to reduce it to a minimum.


### Why is this so important?

At the current moment, humanity consumes more resources than the earth can produce.
One issue with that is, that materials will continuously become more and more rare, leading to increasing prices.
While this is bad, there are even worse things:
The main problem is that the global temperature increases even more every year because of this.
Extreme weather conditions, desertification, floods and droughts will be (and are) the result of this.


## What can I do about my ecological footprint?

First, start changing your own life.
Making a change is all about *you*, so get up and try to live more aware.
Not only cars produce CO2; a big chunk is about general consumerism.
Try the following things:

- Leave your car at home, and go by bus, train, bike or feet.
  Travel together with other people and build carpools.
- Turn down your heater, as well as you air conditioner.
  If you can't completely live without it, at least turn it down.
- Try to avoid taking baths, and keep your showers as cold and short as possible.
- Don't use plastic bags!
  Bring your own cotton bag or backpack when grocery shopping.
  Those bags make up a huge amount of trash, which has to be burnt or is carelessly disposed in the sea.
- Repair broken objects.
  Repair is often cheaper and more environment-friendly than buying it new.
  Also, you can learn a lot from it and it's fun!
- Buy less!
  The production of new things often takes a lot of energy, which has to come from somewhere.
  Living a minimalistic lifestyle is a great step in the right direction.
- Don't go on holiday in foreign countries.
  Traveling, foremost by airplane, makes up a insane amount of CO2 emission.
  Why not enjoy the local pond and forests?
- If you have to travel large distances, choose the right vehicle.
  One can easily travel overnight with a remote bus or train, and avoid a cost- and CO2-expensive flight.
- [Compensate your emissions]({{ site.baseurl }}{% post_url 2018-07-11-co2-compensation %}).
  While reducing them is still the better option, a compensation will cover most of it by donating to NGOs (such as [atmosfair](https://www.atmosfair.de/en)).
- Reduce time spent on electronic devices.
  This will also benefit your general health!
- Recycle the garbage you produce, such as paper, plastic, aluminum etc.
  If you have a garden, start composting.
- Eat less meat, and rather eat vegetarian.
  Meat has very high production energy and water costs.

While those are a quite a few things you can do in order to change your environmental influence, there are still plenty of options I have not mentioned here.
Most importantly, be aware of what you consume and do.
Do I really need to eat my ice cream with a plastic spoon?
Can I really not drink my cola without a straw?
It's also the little things that make an impact.

You should also raise awareness.
Many people know about this problem, but still don't take it seriously enough.
They also don't really notice the small things polluting the environment.


## Conclusion

The global warming can't and won't be stopped by one person.
One might think that ones life is only a "drop on the hot stone", but every single bit matters.
Damaging the climate just a little less is not enough!
A rapid change has to be made by many people.

While raising awareness and pointing out the little things is one big step in the right direction, one can't stop there.
Travelling by plane because it is faster, driving to work with the car because it is more comfortable just aren't suitable arguments anymore - and we, as united humanity, can't afford it.

So grab yourself by your own nose and start waking up.
The proposed changes are difficult, but *not impossible*.
And, it is an absolute necessity that we *constantly get reminded* of the difference that needs to be made.
Otherwise, people will (and *do*, as one can see) forget about it and fall back into their old habits.

So, let us - *together* - make a difference.
We can stop the global warming, if we start and keep working together as one.
For that our planet earth retains the beauty it has.

---
layout: post
title: "Interactive staging of changes with Git"
date: 2019-12-19
category: linux
image: "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/1000px-Git-logo.svg.png"
---

Git is an omnipresent and very useful tool.
In fact even this website is deployed using git.
But sometimes, you just forget to commit, and after more and more lines of code you can't put it all into one commit.
The interactive staging tool is a great help in this case, and will improve your workflow tremendously.

<!--more-->


## Best-practise commits

If you've been using git for some time (which if you don't have, I strongly suggest doing!), you'll probably also know about the best practises associated with it.
For example, you should always do commits as small as possible, so that a bugfix / new feature / ... can be associated with a certain update to the repository.
This is also closely related to [the KISS paradigm]({{ site.baseurl }}{% post_url 2018-05-14-extreme-programming-right-way %}):
Always do as few as possible, but as much as needed.

But in reality, this often doesn't work.
While you're in your thoughts and just coding what comes to your mind, you often don't think about committing your changes.
Who of us isn't guilty of writing commit messages like "update", "bugfix", or even something like "wioqfh"?

The reason I see most frequently behind such messages are two things:

1. Not thinking about what you actually did
2. Being unable to write a specific message because it actually should be two separate commits.

The first reason can be solved easily and is actually a very good thing to do for yourself too.
Thinking about what you actually want to accomplish with this certain update isn't only helpful for looking back in the future,
but also to make yourself aware of why you did things the way you did.

The second one seems to be a bigger issue, because you skipped a commit in the past.
But such mistakes can be corrected quite easily too:
This is where the interactive staging feature of git shows its value.


## How interactive staging works

Using the command `git add -i`, you can start the interactive mode of git.
This will at first show you a list of updated files, similar to `git status`.
Here, you can select what you want to do.

For example, using `[u]pdate`, you can add complete files to your staging area.
With `[r]evert`, you unstage files respectively, `[a]dd` will add untracked files and
using `[d]iff`, you can show the current diff of the files.

`[p]atch` will split the file you want to commit into several small patches.
Then you can decide for each patch if you want to add it, or leave it unstaged.
It is also possible to use the manual staging mode, in which you can edit the staged changes by replacing the + and - lines you see in the history so often.
Removing the - or deleting the line with a + will not stage the change.
This tool is incredibly useful and can help to make fine-grained, very specific commits.

After you're done with staging the changes you want, you can leave the tool using `[q]uit`.
Now you can just `git commit` and you're done splitting your commit into a previous one!


## Split up commits

With use of the interactive staging tool, the option to stage patches and to even manually edit those patches,
it becomes very easy to split commits into multiple ones.
Doing so will help you understand what you did later on, when looking back on your commit history.
It will also make locating bugs a lot easier: If a thing works in one commit but not in the next one, the bug was introduced there.

So, I hope that your commits will be cleaner and more on-point than before. Mine certainly are now.


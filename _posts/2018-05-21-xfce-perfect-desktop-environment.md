---
layout: post
title: "Xfce is the perfect desktop environment: here's why"
date: 2018-05-21
categories: linux
---

With the newly released [18.04 update of Ubuntu]({{ site.baseurl }}{% post_url 2018-04-26-ubuntu-18-04-bionic-beaver-release %}), there is a reason to completely reinstall your computer or play with some settings.
It also the perfect opportunity to change your desktop environment - and here is why you should check out Xfce.

<!--more-->

Xfce is a lightweight desktop environment for Linux, and [Xubuntu as an official flavor](https://www.xubuntu.org), Canonical supports it.
Xfce is less flashy than Gnome or KDE, but far less resource intensive, which makes it the perfect choice for older machines.
But even aside from this nice factor, it is an excellent surface.


## Simplicity

The low resource cost is archived by omitting a lot of effects (like glassy design etc).
While some users might find this ugly or dislike it, I personally really like the feeling achieved by this.
Fast-loading applications, easy-to-use menus and a simplistic desktop are just what I need.
The menu bars can be fully customized or even removed completely if desired.
Also, it is possible to add an infinite amount, which can hold application launchers, information panels or settings.

A lot of the critics say that Xfce is too old, clunky and simply not looking good.
While this might have been true in the past, it definitely isn't nowadays.
One can replace the icon- and menu-design with e.g. the numix icon set, which provides a really clean and flat design.
With lightdm as locking screen manager, it is also really easy to change the looks of this one.
In this video, you can see how easily Xfce can look stunningly beautiful:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GR2y0xOIIdI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display: block; margin: 0 auto;"></iframe>


## Customizability

Unity (now, the adjusted Gnome interface) always had this way to hide options from the user.
While Unity certainly is one extreme, most desktop environments are far more customizable.
KDE is an awesome example for that - every single thing is adjustable.
Xubuntu is somewhere in the middle - it offers a lot of options for the average users, whilst not overloading them with too many.
Aside from the classical settings, nearly everything is tweakable.


## Speed

I always knew that Unity was a quite slow DE.
But I didn't even notice it, until I switched to Xfce - it is so blazingly fast, you won't believe it!
Even when I turned on effects such as window compositioning and shadows, the speed still exceeded the one of Unity by far.

Although you can use compiz to add more effects to your desktop, I mostly didn't need that.
But if you want "wobbly windows" or other animations, you can still install it with `sudo apt install compiz`.


## Xfce - the environment for everyone

After using it for a few years now, I still don't know why only few people with strong computers use this DE.
It is fast, simple and adjustable - and for sure beautiful.
If you have a slow machine, Xfce is the go-to choice anyway.
So give it a shot - and install it with your Ubuntu upgrade.
Or - if you're feeling unsure - try it in a virtual machine first and see if it suits your needs.
I for one absolutely love it.

---
layout: post
title: "CO2 compensation: why and how you should do it"
date: 2018-07-11
categories: environment
image: "/media/plant-and-soil-in-hand.jpg"
---

Our environment has become increasingly important in the way we act and think.
With climate change as  an immediate danger, we have to change the way we produce things, as can be seen in renewable energies and the like.
However, this isn't enough.
In a time where emissions exceed the required maximum by far, we have to take matters in our own hands and need to start with CO2 compensation.

<!--more-->


## The problem: way too high emission

Currently, the average person living on earth produces 4,94 tons of CO2 per year.
In europe, the average is 7,13 t, in north america 16,73.
If we keep it that way, we would need more than 1,6 earths.

<img src="/media/coal_powerplant.jpg" alt="Coal power plant" class="image right">

To fix this, we must [reduce our ecological footprint]({{ site.baseurl }}{% post_url 2018-06-19-ecological-footprint %}).
Otherwise, huge environmental changes such as droughts, floods and desertification (just to name a few) will be the result.


## CO2 compensation and reduction

The first and right change is to reduce our emissions.
There's absolutely no way leading around that,  in the long run.
However, such changes take a lot of time to accomplish.
There won't be solar cars and airplanes from one day to another.
Or moving the energy production from fossil fuels to renewable sources can't be done in an instant, and changes have to be made gradually.

Foremost, we have to change our habits.
We won't be able to have fresh beef or pork for lunch on a daily basis.
We can't afford our yearly 4-hour plane ride in our summer holidays anymore.
Reduction is always better than compensation, because compensation is limited, while reduction is not.

But what can we effectively do concerning changes that involve time?
Compensating the current emissions is a great way of dealing with them.
For example, atmosfair offers [a way to offset flight emissions](https://www.atmosfair.de/en/).


## How it works

Atmosfair invests money into projects leading to lower CO2 emissions, mostly on the southern hemisphere (e.g. african states).
For example, they build furnaces to improve the efficiency in comparison to open fires, as well as solar systems and wind parks for those who don't have them yet.

![Renewable energies chart](/media/renewables_chart.jpg){:style="float: left;"}

While one benefit of that is - obviously - the compensation of our own failing, it also has the benefit of delivering new technological advancements to countries who don't have them yet.


## When and how you should do it

Sometimes, CO2 compensation doesn't make sense.
The website of atmosphair has [a great article explaining this](https://www.atmosfair.de/en/standards/good_offsetting_practices/).
However, you can (and probably should) donate to their project anyways.
It is a really good way to start making a difference, and even a small donation will help in that regard.
Of course, we're all not done there - it's still a very long and hard road before us.

---
layout: page
title: "About"
permalink: /about/
---

<span class="image right"><img alt="Lorenz Bung" src="/media/profile.jpg"></span>


This blog is, simply put, about things I care about.
That is a wide range:
It includes professional articles (for example about software development), but also topics of my personal interests such as ecology, essentialism and hobbies.

- living in [Freiburg, Germany](https://www.google.com/maps/place/Freiburg)
- from [Constance, Germany](https://www.google.com/maps/place/Konstanz)
- studying computer science
- minimalist
- loving nature
- Geocacher! Profile is [here](https://coord.info/PRBZR2M)


If you have any thoughts on posts you read here, suggestions or general concerns, please send me an email to [lorenz.bung@googlemail.com](mailto:lorenz.bung@googlemail.com).
The same goes for any bugs you encounter on my pages.
I'm absolutely looking forward to hearing from you!
